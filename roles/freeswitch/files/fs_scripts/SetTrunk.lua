#!/usr/bin/lua
freeswitch.consoleLog("info","Set Trunk Number\n")
--Version 1.0

Trunk = session:getVariable("sip_h_X-Trunk");

if (Trunk ~= nil) and (string.len(Trunk) == 3)  then
   data = "Trunk="..string.sub(Trunk,2)
   session:execute("set",data);
   return
end

Trunk = session:getVariable("sip_from_user");
if (Trunk ~= nil) and (string.len(Trunk) == 3)  then
   data = "Trunk="..string.sub(Trunk,2)
   session:execute("set",data);
   return
end

session:execute("set","Trunk=00");
