#!/usr/bin/lua
freeswitch.consoleLog("info","Convert 8 Digit ANI\n");

NPD0 = session:getVariable("NPD_0");
NPD1 = session:getVariable("NPD_1");
NPD2 = session:getVariable("NPD_2");
NPD3 = session:getVariable("NPD_3");
NPD4 = session:getVariable("NPD_4");
NPD5 = session:getVariable("NPD_5");
NPD6 = session:getVariable("NPD_6");
NPD7 = session:getVariable("NPD_7");
NPD8 = session:getVariable("NPD_8");
NPD9 = session:getVariable("NPD_9");

eightdigitani = argv[1];
if (eightdigitani == nil) then
 freeswitch.consoleLog("crit","No 8 digit ani passed to conversion script!\n");
 return
end

npd = string.sub(eightdigitani,2, 2);  -- from char 2 to char 2

if      (npd == "0") then areacode = NPD0;
elseif  (npd == "1") then areacode = NPD1;
elseif  (npd == "2") then areacode = NPD2;
elseif  (npd == "3") then areacode = NPD3;
elseif  (npd == "4") then areacode = NPD4;
elseif  (npd == "5") then areacode = NPD5;
elseif  (npd == "6") then areacode = NPD6;
elseif  (npd == "7") then areacode = NPD7;
elseif  (npd == "8") then areacode = NPD8;
elseif  (npd == "9") then areacode = NPD9;
else 
 freeswitch.consoleLog("crit","Invalid 8 digit ani passed to conversion script! "..eightdigitani.."\n");
 return
end

tendigitnumber = areacode..string.sub(eightdigitani,3, 9);
freeswitch.consoleLog("info","NENA_CALLBACK= "..tendigitnumber.."\n");

data = "NENA_CALLBACK="..tendigitnumber;
session:execute("set", data);


return




