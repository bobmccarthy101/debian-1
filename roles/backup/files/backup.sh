#!/bin/bash
#==================================================================
# This script is used to backup files
# from this system to the PSAP backup server.
#
# Author Matt Nichols - April 2017 v2.0
#
# Notes: setup places a cron file in /etc/cron.d/backup
# Requires ssh to have config in ./ssh_config/config
# Requires config in ./config/backup (a Bash script)
#==================================================================

VERSION='2.1.0'

# config for MAIN_LV_NAME, MAIN_VG_NAME, SNAPSHOT_SIZE, fn_prebackup, BACKUP_INCLUDE, BACKUP_EXCLUDE
source "$(dirname "$0")/config/backup"

if [ -z "$SNAPSHOT_SIZE" ]; then
	SNAPSHOT_SIZE='32G'
fi

readonly SET_ENV_PROMPT='Please set both $PSAP_SITE_ID and $PSAP_SERVER_ID [in the file /etc/environment]'

fn_setup_env() {
	echo ''
	while true; do
		read -r -p 'Please enter PSAP site ID (e.g. CO-990): ' PSAP_SITE_ID
		if [[ "$PSAP_SITE_ID" =~ [a-zA-Z0-9-]+ ]]; then
			break
		else
			echo 'Invalid format for site ID'
		fi
	done
	echo
	while true; do
		read -r -p 'Please enter PSAP server ID (e.g. CO-990-1): ' PSAP_SERVER_ID
		if [[ "$PSAP_SERVER_ID" =~ [a-zA-Z0-9-]+ ]]; then
			break
		else
			echo 'Invalid format for server ID'
		fi
	done
}

fn_check_env() {
	if [ -z "$PSAP_SITE_ID" -o -z "$PSAP_SERVER_ID" ]; then
		echo
		echo "$SET_ENV_PROMPT" > /dev/stderr
		echo
		exit 1
	fi
}

fn_save_env() {
	local file='/etc/environment.tmp.6gQP7XpG26uyubEE'
	< /etc/environment sed -e '/^PSAP_SITE_ID=/d' -e '/^PSAP_SERVER_ID=/d' > "$file"
	echo 'PSAP_SERVER_ID="'"$PSAP_SERVER_ID"'"' >> "$file"
	echo 'PSAP_SITE_ID="'"$PSAP_SITE_ID"'"' >> "$file"
	mv "$file" /etc/environment
}


readonly SSH_USER="backup_${PSAP_SERVER_ID}"

readonly SSH_IDENTITY="$(realpath "$(dirname "$0")")/ssh_config/backup.sh.key"
readonly SSH="ssh -F $(realpath "$(dirname "$0")")/ssh_config/config psap_server_$PSAP_SERVER_ID@jump ssh "
readonly REMOTE_SERVER="backup"
readonly ADD_USER_SERVER="psap_add_user"
readonly REMOTE_FOLDER="/backups/current/$PSAP_SITE_ID/$PSAP_SERVER_ID"
readonly REMOTE_LOCATION="$REMOTE_SERVER:$REMOTE_FOLDER"

function fn_confirm () {
	local yn
	while true; do
		echo
		read -r -p "$1 [Y/n]? " yn

		case "$yn" in
			[Yy] ) return 0;; # 0 is true here
			[Yy][Ee][Ss] ) return 0;;
			'' ) return 0;;
			[Nn] ) return 1;;
			[Nn][Oo] ) return 1;;
			* ) break;;
		esac
	done
}

fn_backup () {
	fn_check_env
	local INCLUDE1=() EXCLUDE1=()

	# is in config file for this script
	# stuff like dpkg --get-selections can be done here
	fn_prebackup

	if [ -n "$MAIN_VG_NAME" -a -n "$MAIN_LV_NAME" ]; then
		umount '/mnt/root-backup'
		lvremove --force "/dev/$MAIN_VG_NAME/root-backup"
		mkdir -p '/mnt/root-backup'
		lvcreate --permission r --size "$SNAPSHOT_SIZE" --snapshot --name root-backup "/dev/$MAIN_VG_NAME/$MAIN_LV_NAME"
		mount "/dev/$MAIN_VG_NAME/$MAIN_LV_NAME" '/mnt/root-backup'
		pushd '/mnt/root-backup'
	elif [ -n "$VG_NAMES" -a -n "$LV_NAMES" -a -n "$LV_SIZES" -a -n "$LV_MOUNTS" ]; then
		for index in ${!VG_NAMES[*]}; do
			umount "${LV_MOUNTS[$index]}"
			lvremove --force "/dev/${VG_NAMES[$index]}/${LV_NAMES[$index]}-backup"
		done
		# make snapshots as quickly as possible
		for index in ${!VG_NAMES[*]}; do
			lvcreate --permission r --size "${LV_SIZES[$index]}" --snapshot \
				--name "${LV_NAMES[$index]}-backup" "/dev/${VG_NAMES[$index]}/${LV_NAMES[$index]}"
		done
		for index in ${!VG_NAMES[*]}; do
			mkdir -p "${LV_MOUNTS[$index]}"
			mount "/dev/${VG_NAMES[$index]}/${LV_NAMES[$index]}" "${LV_MOUNTS[$index]}"
		done
		lvs
		vgs
		pushd '/'
	else
		pushd '/'
	fi

	rsync --ignore-missing-args -e "$SSH" -avzRHAX --numeric-ids --delete --delete-excluded --stats "${BACKUP_EXCLUDE[@]/#/--exclude=}" \
		"${BACKUP_INCLUDE[@]}" "$REMOTE_LOCATION"

	popd

	if [ -n "$MAIN_VG_NAME" -a -n "$MAIN_LV_NAME" ]; then
		umount '/mnt/root-backup/'
		lvremove --force "/dev/$MAIN_VG_NAME/root-backup"
	elif [ -n "$VG_NAMES" -a -n "$LV_NAMES" -a -n "$LV_SIZES" -a -n "$LV_MOUNTS" ]; then
		lvs
		vgs
		for index in ${!VG_NAMES[*]}; do
			umount "${LV_MOUNTS[$index]}"
			lvremove --force "/dev/${VG_NAMES[$index]}/${LV_NAMES[$index]}-backup"
		done
	fi
}

fn_disp_box () {
	echo "-----------------------------------------------------------------------"
	echo " backup.sh - Version $VERSION"
	echo
	echo " Backs up files from the 9-1-1 server to"
	echo " $REMOTE_LOCATION"
	echo "-----------------------------------------------------------------------"
}

fn_setup() {
	if [ -z "$PSAP_SITE_ID" -o -z "$PSAP_SERVER_ID" ]; then
		fn_setup_env
		fn_save_env
	fi
	echo
	echo "Public IP: $(dig TXT +short o-o.myaddr.l.google.com @ns1.google.com)"
	echo
	while true; do
		echo "PSAP_SITE_ID: $PSAP_SITE_ID"
		echo "PSAP_SERVER_ID: $PSAP_SERVER_ID"
		if ! fn_confirm 'Are the above site and server IDs correct'; then
			fn_setup_env
		else
			fn_save_env
			break
		fi
	done

	fn_check_env

	if ! fn_confirm "Create a new user and ssh key on the PSAP backup server"; then
		return 1
	fi

	local ident_temp="$SSH_IDENTITY.tmp"
	rm -f "$ident_temp" "$ident_temp.pub"
	mkdir -p "$(dirname "$ident_temp")"

	ssh-keygen -t ed25519 -N '' -f "$ident_temp" -C "backup user for $PSAP_SERVER_ID at $PSAP_SITE_ID" > /dev/null 2> /dev/null || \
		ssh-keygen -t rsa -b 4096 -N '' -f "$ident_temp" -C "backup user for $PSAP_SERVER_ID at $PSAP_SITE_ID" > /dev/null || \
		{ printf '%s\n\n' 'Could not generate ssh identity (key)' > /dev/stderr; exit 1; }
	echo
	echo "Login to create PSAP backup account"
	printf '%s\n%s\n%s\n' "$PSAP_SITE_ID" "$PSAP_SERVER_ID" "$(cat $ident_temp.pub)" | \
		ssh -F "$(dirname "$0")/ssh_config/config" psap_add_user@jump add-user || \
		{ printf '%s\n\n' 'Could not create new user, exiting...' > /dev/stderr; exit 1; }

	[ -f "$SSH_IDENTITY" ] && shred "$SSH_IDENTITY"
	rm -f "$SSH_IDENTITY" "$SSH_IDENTITY.pub"
	mkdir -p "$(dirname "$SSH_IDENTITY")"
	mv "$ident_temp" "$SSH_IDENTITY"
	mv "$ident_temp.pub" "$SSH_IDENTITY.pub"

	echo "MAILTO=\"\"
$(( ( RANDOM % 60 ) )) * * * * root $(cd "$(dirname "$0")"; pwd)/$(basename "$0") -b" > /etc/cron.d/backup
	echo 'Added to cron (filename /etc/cron.d/backup)'
}

fn_menu () {
	fn_disp_box
	local selection
	select selection in "Backup files" "Setup" "Exit"; do
		case "$selection" in
			"Backup files" )
				fn_backup;;
			"Exit" )
				exit 0;;
			"Setup" )
				fn_setup;;
		esac

		echo
		break
	done
}

fn_arg_parsing () {
	if [ "$#" -ne 1 ]; then
		echo 'Too many arguments!' > /dev/stderr
		exit 1
	fi

	case "$1" in
		'-h')
			fn_disp_box
			echo
			echo 'Usage: rsync_psap_to_backup.sh [option]'
			echo "Version: $VERSION"
			echo
			echo '-v = Display version'
			echo '-b = Backup files'
			echo '-h = Display help'
			echo '[DEPRECATED] --backup-with-delay = Backs up, but has a random delay first. Used in cron'
			echo
			echo 'If no command-line option is used a menu will display'
			echo
			exit;;
		'-b')
			fn_backup;;
		'--backup-with-delay')
			# delay is done down below, before lock is activated
			fn_backup;;
		*)
			echo "Unknown argument '$1'" > /dev/stderr
			exit 1;;
	esac
}

fn_main () {
	if [ "$#" -gt 0 ]; then
		fn_arg_parsing "$@"
	elif [ "$#" -eq 0 ]; then
		fn_menu
	fi
}

# allow -v to work even if lock is held or script is not ran as root
if [ "$1" = "-v" -a "$#" -eq 1 ]; then
	echo
	echo "Version $VERSION"
	echo
	exit
fi

if [ "$#" -eq 1 -a "$1" = '--backup-with-delay' ]; then
	sleep $(( ( RANDOM % 60 ) ))m
fi

if [ "$(whoami)" != "root" ]; then
	echo
	echo "This script must be run as root (e.g., using 'sudo')"
	echo
	exit 1
fi

(
	flock -n 200 || { printf '\n%s\n\n' 'This script is currently in use' > /dev/stderr; exit 1; }
	fn_main "$@"
) 200>/var/lock/backup.O5Qc8gwYEN5gwOVj.lock
